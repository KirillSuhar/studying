<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class HomeController extends Controller
{
    public function index()
    {
       $data = ['title' => 'Гостевая книга на Laravel 5.1'];
        return view('pages.messages.index',$data);
    }

    public function edit($id)
    {
        return view('pages.messages.edit');
    }
}

@extends('index')

@section('content')
    <h1 class="text-center">Гостевая книга на Laravel</h1>

      @include('_common._form')
        <div class="messages">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <span>Карлсон</span>
                        <span class="pull-right label label-info">17:15:00 / 03.07.2016</span>
                    </h3>
                </div>

                <div class="panel-body">
                    Я спешил к вам, друзья,
                    С жутким нетерпеньем.
                    Я моторчик не зря
                    Смазывал вареньем.
                    У меня за спиной
                    Вертится пропеллер
                    <hr/>
                    <div class="pull-right">
                        <a class="btn btn-info" href="#">
                            <i class="glyphicon glyphicon-pencil"></i>
                        </a>
                        <button class="btn btn-danger">
                            <i class="glyphicon glyphicon-trash"></i>
                        </button>
                    </div>
                </div>
            </div>
    </div>
@stop
@extends('index')

@section('content')
    <h1 class="text-center">Редактирование: Гостевая книга на Laravel</h1>
    @include('_common._form')

@stop